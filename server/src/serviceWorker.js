self.addEventListener('push', function (event) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    const sendNotification = message => {
        var body =  message.body;
        return self.registration.showNotification( message.title, {
            body
        });
    };

    if (event.data) {
        const message = JSON.parse(event.data.text());
        event.waitUntil(sendNotification(message));
    }
});
