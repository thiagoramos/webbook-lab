<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;

// here I'll get the subscription endpoint in the POST parameters
// but in reality, you'll get this information in your database
// because you already stored it (cf. push_subscription.php)

$subscriptionJson = json_decode(file_get_contents('php://input'), true);
$subscription = Subscription::create($subscriptionJson);

$auth = array(
    'VAPID' => array(
        'subject' => 'https://github.com/Minishlink/web-push-php-example/',
        'publicKey' => 'BHJW25tVjIgrOK-ExQ6RTTlynD_4Pq-HI9N2f7eS8qHviFTvVVTPWxHUxkPq3DGpxdtIcU268M2K6xJ0waPXM4g',
        'privateKey' => 'n-h8ZcvwoTFhbqCL_WemMDMljufMT0EvyiRqUI9yX8w', // in the real world, this would be in a secret file
    ),
);

$webPush = new WebPush($auth);

$res = $webPush->sendNotification(
    $subscription,
	json_encode(array( "body" => $subscriptionJson['text'], "title" => $subscriptionJson['title'] )),
    true
);

// handle eventual errors here, and remove the subscription from your server if it is expired
