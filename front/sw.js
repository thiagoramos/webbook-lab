'use strict';
self.addEventListener('push', function (event) {
		if (!(self.Notification && self.Notification.permission === 'granted')) {
				return;
		}

		const sendNotification = message => {
				var body =  message.body;
				return self.registration.showNotification( message.title, {
						body
				});
		};

		if (event.data) {
				const message = JSON.parse(event.data.text());
				event.waitUntil(sendNotification(message));
		}
});


self.addEventListener('notificationclick', function(event) {
		console.log('[Service Worker] Notification click Received.');

		event.notification.close();

		event.waitUntil(
				clients.openWindow('https://developers.google.com/web/')
		);
});
